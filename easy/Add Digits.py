num = 38

def addDigits(num: int) -> int:
    if num < 10:
        return num
    else:
        r = [int(i) for i in ''.join(str(num))]
        if len(r) > 1:
            t = sum([int(i) for i in ''.join(str(num))])
            return addDigits(t)
        else:
            return sum([int(i) for i in ''.join(str(num))])

print(addDigits(num))

